<?php

namespace Modgento\Twitter\Block;

class Twitter extends \Magento\Framework\View\Element\Template
{

    public $tweetObject;
    public $params;
    protected $scopeConfig;
    private $url;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    ){
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
        $this->getTweets();
    }

    public function getTweets()
    {
        $this->initialise();
        $response = new \Abraham\TwitterOAuth\TwitterOAuth(
            $this->params['consumer_key'],
            $this->params['consumer_secret'],
            $this->params['oauth_access_token'],
            $this->params['oauth_access_token_secret']
        );
        $this->tweetObject = $response->get('statuses/user_timeline', [
            'count' => $this->count,
            'screen_name' => $this->username
        ]);
    }

    public function getTweetCount()
    {
        return (int)$this->scopeConfig->getValue('modgento/twitter/count', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function initialise()
    {
        $this->username = $this->scopeConfig->getValue('modgento/twitter/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->count = $this->getTweetCount();
        $this->url = 'https://api.twitter.com/1.1/statuses/home_timeline.json?screen_name=' . $this->username . '&count=' . $this->count;
        $this->params = array(
            'oauth_access_token' => $this->scopeConfig->getValue('modgento/twitter/token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'oauth_access_token_secret' => $this->scopeConfig->getValue('modgento/twitter/token_secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'consumer_key' => $this->scopeConfig->getValue('modgento/twitter/consumer_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'consumer_secret' => $this->scopeConfig->getValue('modgento/twitter/consumer_secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
        );
    }
}
